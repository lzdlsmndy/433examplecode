package net.jcip.examples;

import java.util.*;

public class SequentialPuzzleSolver<POS_REP, MOVE_REP> {
	private final IPuzzle<POS_REP, MOVE_REP> puzzle;
	private final Set<POS_REP> seen = new HashSet<POS_REP>();

	public SequentialPuzzleSolver(IPuzzle<POS_REP, MOVE_REP> puzzle) {
		this.puzzle = puzzle;
	}

	/**
	 * 
	 * @return The list of nodes on path from initial position to goal  
	 */
	public List<MOVE_REP> solve() {
		POS_REP pos = puzzle.initialPosition();
		return search(new PuzzleNode<POS_REP, MOVE_REP>(pos, null, null));
	}

	
	/**
	 * 
	 * @param node The current node
	 * @return The list of nodes on path from initial position to goal 
	 */
	
	private List<MOVE_REP> search(PuzzleNode<POS_REP, MOVE_REP> node) {
		// Have we already seen this position?
		if (!seen.contains(node.pos)) {
			System.out.println("evaluting position:" + node.pos);
			seen.add(node.pos);
			// Are we at the goal position?
			if (puzzle.isGoal(node.pos)) {
				System.out.println("Solution Found:" + node.pos);
				// reconstruct solution backwards from goal position
				return node.asMoveList();
			}
			// Not at the goal position. Search forward over all possible moves 
			for (MOVE_REP move : puzzle.legalMoves(node.pos)) {
				POS_REP pos = puzzle.move(node.pos, move);
				PuzzleNode<POS_REP, MOVE_REP> child = new PuzzleNode<POS_REP, MOVE_REP>(pos, move, node);
				List<MOVE_REP> result = search(child);
				if (result != null)
					return result;
			}
		}
		return null;
	}
}

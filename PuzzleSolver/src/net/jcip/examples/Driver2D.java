package net.jcip.examples;

import java.awt.Point;

public class Driver2D {
	public static void main(String[] args) throws InterruptedException {
		SimpleMaze sp = new SimpleMaze();
		SequentialPuzzleSolver<Point, Move2DEnum> solver = new SequentialPuzzleSolver<Point, Move2DEnum>(sp);
		// ConcurrentPuzzleSolver<Point, Move2DEnum> solver = new ConcurrentPuzzleSolver<Point, Move2DEnum>(sp);
		// PuzzleSolver<Point, Move2DEnum> solver = new PuzzleSolver<Point, Move2DEnum>(sp);
		
		System.out.println(solver.solve());
	}
}

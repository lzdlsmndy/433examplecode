package edu.umd.cs.cmsc433.ConcurrentCollections;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierEx1 {

	public static void main(String[] args) throws Exception {
		int threads = 3;
		final CyclicBarrier barrier = new CyclicBarrier(threads);

		for (int i = 0; i < threads; i++) {
			new Thread(new Runnable() {
				public void run() {
					try {
						log("At run()");
						barrier.await();

						log("Do work");

						// comment out to remove synchronization point
						barrier.await();

						log("Wait for end");
						barrier.await();

						log("Done");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	private static void log(String msg) {
		System.out.println(System.currentTimeMillis() + ": "
				+ Thread.currentThread().getId() + "  " + msg);
	}
}

import java.util.Arrays;

import jsr166y.ForkJoinPool;
import jsr166y.RecursiveTask;

public class ParallelToLowerCase extends RecursiveTask<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mSource;
	public ParallelToLowerCase(String src) {
		mSource = src;
	}

	protected String computeDirectly() {
		return mSource.toLowerCase();
	}

	protected static int sThreshold = 46;

	protected String compute() {
		System.out.println("length:" + mSource.length());
		if (mSource.length() < sThreshold) {
			return computeDirectly();
		}

		ParallelToLowerCase p1 = new ParallelToLowerCase(mSource.substring(0, mSource.length()/2));
		p1.fork();
		ParallelToLowerCase p2 = new ParallelToLowerCase(mSource.substring(mSource.length()/2,mSource.length()));
		//String tmp =  p1.join() + p2.compute();
		p2.fork();
		String tmp =  p1.join() + p2.join();
		return tmp;
	}

	public static void main(String[] args) {

		final String list = Arrays.deepToString(new String[] { "AB", "CD", "EF", "GH", "MN",
				"RM", "JP", "LS", "QP", "TX", "ST", "SZ", "AA", "PQ", "RS",
				"RM", "JP", "LS", "QP", "TX", "ST", "SZ", "AA", "PQ", "RS",
				"RM", "JP", "LS", "QP", "TX", "ST", "SZ", "AA", "PQ", "RS",
				"RM", "JP", "LS", "QP", "TX", "CO", "BA", "MP", "AM", "FF" });

		ForkJoinPool pool = new ForkJoinPool();

		ParallelToLowerCase whole = new ParallelToLowerCase(list.toString());
		System.out.println(pool.invoke(whole));

	}

}
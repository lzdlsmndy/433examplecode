import java.util.Random;

import jsr166y.ForkJoinPool;
import jsr166y.RecursiveAction;

public class MaxSolver extends RecursiveAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final static int THRESHOLD = 50000;
	private final MaxProblem problem;
	private int result;
	
	public MaxSolver(MaxProblem prob) {
		this.problem = prob;
	}
	
	@Override
	protected void compute() {
		if (problem.size() < THRESHOLD) {
			result = problem.solveSequentially();
		}
		else {
			int m = problem.size() / 2;
			MaxSolver left, right;
			left = new MaxSolver(problem.subproblem(0, m));
			right = new MaxSolver(problem.subproblem(m,problem.size()));

			// blocks until left.isDone() and right.isDone()
			invokeAll(left,right);
			
			result = Math.max(left.result,right.result);
		}
	}
	
    /**
     * Creates an array of the given size with random contents.
     */
    static private Random rand = new Random();
    public static int[] createRand(int num) {
        int[] result = new int[num];
        for(int i = 0 ; i < num; i++)
            result[i] = rand.nextInt(1000);
        return result;
    }

	public static void main(String args[]) {
		int nThreads = 2;
		int arrSz = 10000000;
		int[] array = createRand(arrSz);
		MaxProblem prob = new MaxProblem(array,0,arrSz);
		long startTm = System.currentTimeMillis();
		int result = prob.solveSequentially();
		long endTm = System.currentTimeMillis();
		System.out.println("SEQ: max is "+result+" time ="+(endTm-startTm)+" ms");
		ForkJoinPool pool = new ForkJoinPool(nThreads);
		MaxSolver solver = new MaxSolver(prob);
		startTm = System.currentTimeMillis();
		pool.invoke(solver);
		endTm = System.currentTimeMillis();
		System.out.println("FJ: max is "+solver.result+" time ="+(endTm-startTm)+" ms");
	}
}
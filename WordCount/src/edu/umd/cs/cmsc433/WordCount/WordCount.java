package edu.umd.cs.cmsc433.WordCount;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/** The original, sequential program. */

public class WordCount implements Runnable {
    private String buffer;
    private Map<String,Integer> counts;

    private final static boolean printAll = false;

    public WordCount(String buffer, Map<String,Integer> counts) {
        this.counts = counts;
        this.buffer = buffer;
    }

    private static String readFileAsString(BufferedReader reader, int size)
        throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(size);
        int numRead=0;

        while(size > 0) {
            int bufsz = 1024 > size ? size : 1024;
            char[] buf = new char[bufsz];
            numRead = reader.read(buf,0,bufsz);
            if (numRead == -1)
                break;
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            size -= numRead;
        }
        return fileData.toString();
    }

    /**
     * Main task : tokenizes the given buffer and counts words. 
     */
    public void run() {
        StringTokenizer st = new StringTokenizer(buffer," :;,.{}()\t\n");
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            Integer oldCount = counts.remove(token);
            if (oldCount != null)
                counts.put(token,oldCount + 1);
            else
                counts.put(token,1);
        }
    } 

    public static void main(String args[]) throws java.io.IOException {
        long startTime = System.currentTimeMillis();
        if (args.length != 1) {
            System.out.println("Usage: <file>\n");
	    System.exit(1);
        } 
        File f = new File(args[0]);
        BufferedReader reader = new BufferedReader(new FileReader(f));
        long len = f.length();
        if (len > Integer.MAX_VALUE) {
            System.out.println("Can't handle file of size "+len);
            System.exit(1);
        }
        String toCount = readFileAsString(reader,(int)f.length());
        Map<String,Integer> m = new HashMap<String,Integer>();
        new WordCount(toCount,m).run();
        long endTime = System.currentTimeMillis();
        long elapsed = endTime - startTime;
        int total = 0;
            for (Map.Entry<String,Integer> entry : m.entrySet()) {
                int count = entry.getValue();
                if (printAll)
                    System.out.format("%-30s %d\n",entry.getKey(),count);
                total += count;
            }
        System.out.println("Total words = "+total);
        System.out.println("Total time = "+elapsed+" ms");
    }
}
package DeadLock;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * LeftRightDeadlock
 *
 * Simple lock-ordering deadlock
 *
 * @author Brian Goetz and Tim Peierls
 */

public class LeftRightDeadlock {
	private final Object left = new Object();
	private final Object right = new Object();

	public void leftRight() {
		synchronized (left) {
			synchronized (right) {
				doSomething();
			}
		}
	}

	public void rightLeft() {
		synchronized (right) {
			synchronized (left) {
				doSomethingElse();
			}
		}
	}

	void doSomething() {
	}

	void doSomethingElse() {
	}

	public static void main(String[] args) {
		ExecutorService exec = Executors.newCachedThreadPool();
		final LeftRightDeadlock lr = new LeftRightDeadlock();

		exec.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {
					lr.leftRight();
					System.out.println("done");
				}
			}
		});

		exec.execute(new Runnable() {
			@Override
			public void run() {
				while (true) {
					lr.rightLeft();
					System.out.println("done");
				}
			}
		});

		exec.shutdown();
	}
}

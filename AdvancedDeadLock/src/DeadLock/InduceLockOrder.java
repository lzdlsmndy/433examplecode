package DeadLock;

/**
 * InduceLockOrder
 * 
 * Inducing a lock order to avoid deadlock
 * 
 * @author Brian Goetz and Tim Peierls
 */
public class InduceLockOrder {
	private static final Object tieLock = new Object();

	static public void transfer(Account fromAcct, Account toAcct,
			DollarAmount amount) throws InsufficientFundsException {
		if (fromAcct.getBalance().compareTo(amount) < 0)
			throw new InsufficientFundsException();
		else {
			fromAcct.debit(amount);
			toAcct.credit(amount);
		}
	}

	static public void transferMoney(final Account fromAcct,
			final Account toAcct, final DollarAmount amount)
			throws InsufficientFundsException {

		int fromHash = System.identityHashCode(fromAcct);
		int toHash = System.identityHashCode(toAcct);

		if (fromHash < toHash) {
			synchronized (fromAcct) {
				synchronized (toAcct) {
					transfer(fromAcct, toAcct, amount);
				}
			}
		} else if (fromHash > toHash) {
			synchronized (toAcct) {
				synchronized (fromAcct) {
					transfer(fromAcct, toAcct, amount);
				}
			}
		} else {
			synchronized (tieLock) {
				synchronized (fromAcct) {
					synchronized (toAcct) {
						transfer(fromAcct, toAcct, amount);
					}
				}
			}
		}
	}

}

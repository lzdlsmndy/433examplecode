package cmsc433.rmichat.client;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import cmsc433.rmichat.common.Client;
import cmsc433.rmichat.common.Connection;
import cmsc433.rmichat.common.Server;

/**
  * A text based RMI chat client.
  */
public class ClientImpl  extends UnicastRemoteObject
        implements Client {

	private static final long serialVersionUID = 1L;
	public ClientImpl () throws RemoteException {}
	
        public void usersChanged() throws java.rmi.RemoteException {}

        public void wasSaid(String who, String msg) throws java.rmi.RemoteException
		{
		System.out.println(who + ": " + msg);
		}
	public static void main(String[] args) throws Exception {
 if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
        }
		Client c = new ClientImpl();
		Server  s= (Server)Naming.lookup("rmi://" + args[1] + ":1099/ChatServer");
		Connection conn = s.logon(args[0],c);
		if (conn == null) return;
		BufferedReader in =
			new BufferedReader(new InputStreamReader(System.in));
		String msg = null;
		inputLoop: while ((msg = in.readLine()) != null) {
			directedMsg: if (msg.charAt(0) == '(') {
				int i = msg.indexOf(')'); 
				if (i == -1) break directedMsg;
				String toWho = msg.substring(1,i-1);
				String contents 
				   = msg.substring(i+1,msg.length()-1).trim();
				conn.say(toWho,contents);
				continue inputLoop;
				}
			conn.say(msg);
		}
		conn.logoff();
		}
}

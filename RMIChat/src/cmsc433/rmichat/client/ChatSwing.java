package cmsc433.rmichat.client;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cmsc433.rmichat.common.Client;
import cmsc433.rmichat.common.Connection;
import cmsc433.rmichat.common.Server;

/**
 * A Swing based RMI chat client
 */
public class ChatSwing extends Applet {

	private static final long serialVersionUID = 1L;
	String name;
	Client c;
	Server s;
	Connection conn;

	final Color standardButtonColor = Color.red;
	final Color selectedButtonColor = Color.green;

	Hashtable<String, JButton> buttons = new Hashtable<String, JButton>();

	String confidential = null;
	JTextArea transcript = new JTextArea("Transcript:\n", 20, 70);

	JTextField msg = new JTextField(50);

	JButton quit = new JButton("Quit");

	JPanel panel = new JPanel();
	JPanel whoPanel = new JPanel();

	PersonButtonListener pbl = new PersonButtonListener();

	class ChatClientImpl extends UnicastRemoteObject implements Client {

		private static final long serialVersionUID = 1L;

		public ChatClientImpl() throws RemoteException {
		}

		public void usersChanged() throws java.rmi.RemoteException {
			updateWho();
		}

		public void wasSaid(String who, String msg)
				throws java.rmi.RemoteException {
			String o = who + ": " + msg + "\n";
			if (transcript != null)
				transcript.append(o);
			else
				System.out.print(o);
		}
	}

	class PersonButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String s = e.getActionCommand();
			if (confidential != null && confidential.equals(s)) {
				JButton b = buttons.get(confidential);
				if (b != null)
					b.setBackground(standardButtonColor);
				confidential = null;
			} else {
				JButton b;
				if (confidential != null) {
					b = buttons.get(confidential);
					if (b != null)
						b.setBackground(standardButtonColor);
				}
				confidential = s;
				b = buttons.get(confidential);
				if (b != null)
					b.setBackground(selectedButtonColor);
			}
		}
	}

	public ChatSwing(String nm, String serverHost) {
		name = nm;
		try {
			c = new ChatClientImpl();
			System.out.println("got c=" + c);
			s = (Server) Naming.lookup("//" + serverHost + "/ChatServer");
			System.out.println("got s=" + s);
			conn = s.logon(name, c);
			System.out.println("got conn=" + conn);
			synchronized (this) {
				notify();
			}
		} catch (Exception e) {
			System.err.println("Error starting applet:" + e);
			c = null;
			s = null;
			conn = null;
		}
	}

	public void init() {
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		add(new JScrollPane(transcript), BorderLayout.CENTER);
		transcript.setEditable(false);
		panel.setLayout(new FlowLayout());
		add("South", panel);
		whoPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		add("North", whoPanel);
		// add("South",msg);
		panel.add(new Label(name + ":"));
		panel.add(msg);
		msg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (confidential != null)
						conn.say(confidential, e.getActionCommand());
					else
						conn.say(e.getActionCommand());
				} catch (Exception err) {
					System.out.println("Error while saying. error: " + err);
				}
				msg.setText("");
			}
		});
		// add("East",quit);
		quit.setMinimumSize(quit.getPreferredSize());
		panel.add(quit);
		quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doQuit();
			}
		});
	}

	synchronized void doQuit() {
		try {
			conn.logoff();
			System.exit(0);
		} catch (Exception err) {
			System.out.println("Error while trying to quit. " + err);
		}
		;
	}

	synchronized void updateWho() {
		while (conn == null) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("Interrupted?? " + e);
			}
		}
		whoPanel.removeAll();
		try {
			String[] w = conn.users();
			for (int i = 0; i < w.length; i++) {
				if (!w[i].equals(name)) {
					JButton b = buttons.get(w[i]);
					if (b == null) {
						b = new JButton(w[i]);
						b.setMinimumSize(b.getPreferredSize());
						// System.out.println("Preferred size for " + w[i] +
						// " button is " + b.getPreferredSize());
						// System.out.println("size for " + w[i] + " button is "
						// + b.getSize());
						b.setBackground(standardButtonColor);
						b.addActionListener(pbl);
						buttons.put(w[i], b);
					}
					;
					whoPanel.add(b);
				}
			}
		} catch (Exception err) {
			System.out.println("Error while updating who. " + err);
			err.printStackTrace(System.out);
		}
		;
		whoPanel.setSize(whoPanel.getPreferredSize());
		whoPanel.validate();
		validate();
	}

	public static void main(String[] args) throws Exception {

		final ChatSwing applet = new ChatSwing(args[0], args[1]);
		if (applet.conn == null)
			return;
		JFrame aFrame = new JFrame("Chat Client for " + args[0]);
		aFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				applet.doQuit();
			}
		});
		aFrame.getContentPane().add(applet, BorderLayout.CENTER);
		applet.setVisible(true);
		applet.validate();

		applet.validate();
		aFrame.validate();
		applet.init();
		aFrame.setVisible(true);
		aFrame.pack();
		applet.start();
		// aFrame.setSize(700,400);
	}
}

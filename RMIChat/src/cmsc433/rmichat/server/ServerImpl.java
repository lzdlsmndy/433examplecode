package cmsc433.rmichat.server;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import cmsc433.rmichat.common.Client;
import cmsc433.rmichat.common.Connection;
import cmsc433.rmichat.common.Server;

/** Implementation of Server */
public class ServerImpl extends UnicastRemoteObject implements Server {
	
	private static final long serialVersionUID = 1L;
	Map<String, Client> connections = new HashMap<String, Client>();

	public ServerImpl() throws RemoteException {
	}

	public void notifyWhoChanged() {
		// OK. We had to fix a little problem here.
		// We need to tell all connections that a new user has logged on
		// However, the user logging on might not be fully established,
		// and be unable to accept the message that whoChanged until
		// such time as the log-on is complete.
		// We can fix this either Server side or Client Side.
		// The Server side fix is to run this in a seperate thread,
		// so that if it blocks, we won't delay the return from logon
		// (which could create deadlock)
		(new Thread() {
			public void run() {
				Map<String, Client> sendTo;
				synchronized (connections) {
					sendTo = new HashMap<String, Client> (connections);
				}
				for (String who : sendTo.keySet()) {
					Client c = sendTo.get(who);
					try {
						c.usersChanged();
					} catch (RemoteException err) {
						System.out.println("Dropping " + who
								+ " due to remote error");
						connections.remove(who);
					}
				}
			}
		}).start();
	}

	public Connection logon(String name, Client c)
			throws java.rmi.RemoteException {
		Connection conn;
		synchronized (connections) {
			if (name.equals("Server") || connections.get(name) != null) {
				c.wasSaid("Server", "Sorry, but that name is already in use");
				return null;
			}
			conn = new ConnectionImpl(name, c, this, connections);
			connections.put(name, c);
		}
		c.wasSaid("Server", "Welcome");
		notifyWhoChanged();
		return conn;
	}

	public static void main(String args[])
        {
        	// Create and install a security manager
			if (System.getSecurityManager() == null) System.setSecurityManager(new RMISecurityManager());
            
                try {
                		Registry registry = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
                        ServerImpl obj = new ServerImpl();
                        registry.rebind("ChatServer", obj);
			            System.out.println("ChatServer bound in registry on port:" + args[0]);
                } catch (Exception e) {
                        System.out.println("ChatServerImpl err: " + e.getMessage());
                        e.printStackTrace();
                }
        }
}

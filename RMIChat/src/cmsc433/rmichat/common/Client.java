package cmsc433.rmichat.common;
/**
 * The interface for an RMI Chat client
 */
public interface Client extends java.rmi.Remote {
	/**
	 * Called by server when msg is sent to the client
	 * @param who The person who sent the message
	 * @param msg The message
	 */
        public void wasSaid(String who, String msg) throws java.rmi.RemoteException;
	/**
	 * Called by server when the set of people in the room changes.
	 * The client should invoke the who() method of the connect to get
	 * the new list.
	 */
	public void usersChanged() throws java.rmi.RemoteException;
}

package edu.umd.cs.cmsc433.HashMapPerf;


import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

// NOT THREAD-SAFE

public class HashMapPerfTest {
	// private Map<Integer, Integer> queryCounts = Collections.synchronizedMap(new HashMap<Integer, Integer>(1000)); // synchronized
	private Map<Integer, Integer> queryCounts = new ConcurrentHashMap<Integer, Integer>(1000);  // concurrent

	private void incrementCount(Integer q) {
		Integer cnt = queryCounts.get(q);
		if (cnt == null) {
			queryCounts.put(q, 1);
		} else {
			queryCounts.put(q, cnt + 1);
		}
	}

	public static void main(String[] args) throws InterruptedException {
		final int numWorkers = 20;
		final HashMapPerfTest t = new HashMapPerfTest();
		final CountDownLatch start = new CountDownLatch(1);
		final CountDownLatch end = new CountDownLatch(numWorkers);

		Runnable r = new Runnable() {
			public void run() {
				Random r = new Random();
				try {
					start.await();
				} catch (InterruptedException e) {}
				for (int i = 0; i < 100000; i++) {
					t.incrementCount(r.nextInt(numWorkers-1));
				}
				end.countDown();
			}
		};
		for (int i = 0; i < numWorkers; i++) {
			new Thread (r).start();
		}
		
		start.countDown();
		long startTime = System.currentTimeMillis();
		end.await();
		System.out.println("Elapsed Time:"
				+ (System.currentTimeMillis() - startTime));
	}
}
package edu.umd.cs.cmsc433.Alarms;

import java.io.*;

public class ThreadTestSync {
	private static BufferedReader b = 
			new BufferedReader(new InputStreamReader(System.in));
	private static String msg = null;
	private static int timeout = 0;

	class AlarmSync {
		String msg = null;
		int timeout = 0;

		AlarmSync(String msg, int timeout) {
			this.msg = msg;
			this.timeout = timeout;
		}
	}

	private void parseInput(String line) {
		try {
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ' ') {
					timeout = Integer.parseInt(line.substring(0, i));
					msg = line.substring(i + 1);
					break;
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("bad input " + line);
		}
	}

	private void doAlarm() throws IOException {
		while (true) {
			System.out.print("Alarm> ");
			String line = b.readLine();
			if (line == null)
				return;
			parseInput(line);
			if (msg == null)
				continue;
			try {
				Thread.sleep(timeout * 1000);
			} catch (InterruptedException e) {
			}
			System.out.println("(" + timeout + ") " + msg);
		}
	}

	public static void main(String args[]) throws IOException {
		System.out
		.println("Enter alarms of the form `time msg', e.g., `3 hello'");
		ThreadTestSync tts = new ThreadTestSync();
		tts.doAlarm();
	}
}

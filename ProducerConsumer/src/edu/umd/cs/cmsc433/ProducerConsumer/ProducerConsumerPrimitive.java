package edu.umd.cs.cmsc433.ProducerConsumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;

public class ProducerConsumerPrimitive {
	static int num;
	final static CountDownLatch start = new CountDownLatch(1);

	static synchronized int getNext() {
		return ++num;
	}

	public static void main(String[] args) throws InterruptedException {
		Buffer c = new Buffer(10);
		Producer p1 = new Producer(c, 1);
		Consumer c1 = new Consumer(c, 1);
		Consumer c2 = new Consumer(c, 2);
		Consumer c3 = new Consumer(c, 3);
		Thread t1 = new Thread(p1);
		t1.start();
		Thread t2 = new Thread(c1);
		t2.start();
		Thread t3 = new Thread(c2);
		t3.start();
		Thread t4 = new Thread(c3);
		t4.start();

		start.countDown();
		Thread.sleep(500);

		t1.interrupt();
		t2.interrupt();
		t3.interrupt();
		t4.interrupt();
	}

	static class Buffer {
		private Queue<Integer> contents;
		private int capacity;

		public Buffer(int capacity) {
			this.contents = new LinkedList<Integer>();
			this.capacity = capacity;
		}

		public synchronized int get() {
			boolean interrupted = false;

			while (contents.size() <= 0 && !interrupted) {
				try {
					wait();
				} catch (InterruptedException e) {
					System.out.println("Interrupted in get()");
					interrupted = true;
				}
			}

			notifyAll();

			if (interrupted) {
				Thread.currentThread().interrupt();
				return -1;
			} else {
				return contents.remove();
			}
		}

		public synchronized void put(int value) {
			boolean interrupted = false;

			while (contents.size() >= capacity && !interrupted) {
				try {
					wait();
				} catch (InterruptedException e) {
					System.out.println("Interrupted in put()");
					interrupted = true;
				}
			}
			if (interrupted) {
				Thread.currentThread().interrupt();
			} else {
				contents.add(value);
			}
			notifyAll();
		}
	}

	static class Consumer implements Runnable {
		private Buffer buffer;
		private int number;

		public Consumer(Buffer c, int number) {
			buffer = c;
			this.number = number;
		}

		public void run() {
			try {
				start.await();
			} catch (InterruptedException e1) {
			}
			while (!Thread.interrupted()) {
				System.out.println("Consumer #" + this.number + " got: "
						+ buffer.get());
			}
			System.out.println(this + ":exited Consumer");
		}
	}

	static class Producer implements Runnable {
		private Buffer buffer;
		private int number;

		public Producer(Buffer c, int number) {
			buffer = c;
			this.number = number;
		}

		public void run() {
			int i;
			try {
				start.await();
			} catch (InterruptedException e1) {
			}
			while (!Thread.interrupted()) {
				i = ProducerConsumerPrimitive.getNext();
				buffer.put(i);
				System.out.println("Producer #" + this.number + " put: " + i);
			}
			System.out.println(this + ":exited Producer");
		}
	}
}
